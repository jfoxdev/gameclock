#ifndef FRAME_RATE_MANAGER_H
#define FRAME_RATE_MANAGER_H

#include "GameClock.h"
#include "Timeline.h"
#include "Timer.h"

#include <chrono>
#include <thread>

namespace VGTK {

class FrameRateManager
{
  public:
	FrameRateManager(unsigned int targetfps, bool cap = true) 
	{
		//every second update the Actual FPS
		mFrameClock = new Timer(1000);
		mActualFrameCount = 0;
		mTimelineId = GameClock::Instance()->NewTimeline();
		mCurrent = GameClock::Instance()->getNow(mTimelineId);
		mTargetFPS = targetfps;
		mCap = cap;
		mDelta = std::chrono::milliseconds(0);
		mTotalDelta = std::chrono::milliseconds(0);
		mMotionDelta = 0;
		mFrameCount = 0;
		mActualFPS = 0;
	}
	FrameRateManager()
	{
		//default framerate
		FrameRateManager(30);
	}
	~FrameRateManager() {}
	
	void Start()
	{
		mCurrent = GameClock::Instance()->getNow(mTimelineId);
	}
	void Stop()
	{
		mLast = mCurrent;
		mCurrent = GameClock::Instance()->getNow(mTimelineId);
		mDelta = std::chrono::duration_cast<std::chrono::milliseconds>(mCurrent - mLast);
		mTotalDelta += mDelta;
		mMotionDelta = (double)mDelta.count() / 1000.0;
		
		//framesleep
		if((isCapped()) && ((mDelta.count()) <  1000/mTargetFPS)) 
		{ 
			//Sleep the remaining frame time 
			mSleepTime = std::chrono::milliseconds((1000 / mTargetFPS) - mDelta.count()); 
			std::cout<<"SLEEPFOR: "<<mSleepTime.count()<<"ms"<<std::endl;
			Sleep(mSleepTime);		

		}
		mActualFrameCount++; 
		mFrameCount++;
		
		if(mFrameCount >= mTargetFPS) 
		{
			mFrameCount = 0; 
		}
		mFrameClock->Update();
		if(mFrameClock->isExpired())
		{
			std::cout<<"Actual FPS: "<<mActualFrameCount<<std::endl;
			mActualFrameCount = 0;
			mFrameClock->Reset();
		}

	}
	
	void setTargetFPS(unsigned int fps) { mTargetFPS = fps; }
	unsigned int getTargetFPS() { return mTargetFPS; }
	double getActualFPS() { return mActualFPS; }
	
	bool isCapped() { return mCap; }
	void setCap(bool cap) { mCap = cap; }
	void ToggleCap() { setCap(!isCapped()); }
	
	unsigned int getTimelineId() { return mTimelineId; }
	
	double getMotionDelta() { return mMotionDelta; }

  protected:
	void Sleep(std::chrono::milliseconds ms)
	{
		std::this_thread::sleep_for(ms);
	}

  private:
	unsigned int mTimelineId;
	
	bool mCap;
	
	Timer* mFrameClock;
	unsigned int mActualFrameCount;
	

	std::chrono::time_point<std::chrono::high_resolution_clock> mCurrent;
	std::chrono::time_point<std::chrono::high_resolution_clock>mLast;
	std::chrono::duration<long long, std::milli> mDelta;
	std::chrono::duration<long long, std::milli> mTotalDelta;
	std::chrono::duration<long long, std::milli> mSleepTime;
	double mActualFPS;
	
	unsigned int mTargetFPS;
	unsigned int mFrameCount;
	
	double mMotionDelta;
	
};

}

#endif
