#include "GameClock.h"
#include <iostream>
#include "Timeline.h"
#include "FrameRateManager.h"
#include "Timer.h"

//Vatrix Games Tool Kit
using namespace VGTK;

int main()
{
	
	//initialize the GameClock
	GameClock* gc = GameClock::Instance();
	
	//create a FrameRateManager capped at 60FPS
	FrameRateManager fps(60);
	
	//make a new 10 second timer
	Timer* tmp = new Timer(10000);
	
	//while the Timer is not expired
	while(!tmp->isExpired())
	{
		//frame rate manager start 
		fps.Start();
		
		
		//update the game clock once per loop
		//(syncs system clock with stored game time
		gc->Update();
		//update the timer (or all timers)
		tmp->Update();
		//cap the frame rate manager for the loop
		fps.Stop();
	}
	
	return 0;
}
