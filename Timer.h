#ifndef TIMER_H
#define TIMER_H

#include "GameClock.h"
#include "Timeline.h"

namespace VGTK {
	
class Timer
{
  public:
	//use a new timeline for the timer by default
	Timer(long long int duration,  unsigned int timeline_id = 0)
	{
		if(timeline_id == 0)
		{
			mTimelineId = GameClock::Instance()->NewTimeline();
		}
		else { mTimelineId = timeline_id; }
		
		mStartTime = GameClock::Instance()->getNow(mTimelineId);
		setDuration(duration);
		mExpired = false;		
	}
	Timer() { Timer(0,0); }
	~Timer() {}
	
	void Update() 
	{
		//if now is after the epoch of the timeline + duration
		if (GameClock::Instance()->getNow(mTimelineId) > (mStartTime + mDuration))
		{
			mExpired = true;
		}
	}
	void Reset()
	{
		mExpired = false;
		mStartTime = GameClock::Instance()->getNow(mTimelineId);
	}
	
	bool isExpired() { return mExpired; }
	void setExpired(bool val) { mExpired = val; }
	void setStartTime(std::chrono::time_point<std::chrono::high_resolution_clock> start_time) { mStartTime = start_time; }
	std::chrono::time_point<std::chrono::high_resolution_clock> getStartTime() { return mStartTime; }
	void setDuration(long long int dur) { mDuration = std::chrono::milliseconds(dur); }
	void setTimeline(unsigned int tlid) { mTimelineId = tlid; }
	long long int getDuration() { return mDuration.count(); }
	unsigned int getTimelineId() { return mTimelineId; }
	
	Timer& operator=(Timer& rhs)
	{
		if (this == &rhs) {	return *this; }
		
		
		this->setTimeline(rhs.getTimelineId());
		this->setStartTime(rhs.getStartTime());
		this->setDuration(rhs.getDuration());	
		this->setExpired(rhs.isExpired());
		
		return *this;
	}
  private:
	unsigned int mTimelineId;
	std::chrono::duration<long long, std::milli> mDuration;
	std::chrono::time_point<std::chrono::high_resolution_clock> mStartTime;
	bool mExpired;

};

}

#endif
