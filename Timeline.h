#ifndef TIMELINE_H
#define TIMELINE_H

#include <chrono>

namespace VGTK {

class Timeline
{ 
 private:
	std::chrono::time_point<std::chrono::high_resolution_clock> mEpoch;
	std::chrono::time_point<std::chrono::high_resolution_clock> mNow;
	std::chrono::duration<long long, std::milli> mStep;
	bool mPause;
	bool mSlow;
	
  public:
	Timeline(std::chrono::time_point<std::chrono::high_resolution_clock> epoch,std::chrono::time_point<std::chrono::high_resolution_clock> now, long long int step, bool pause = false, bool slow = false)
	 : mEpoch(epoch), mNow(now), mStep(std::chrono::duration<long long, std::milli>(step)), mPause(pause), mSlow(slow) {}
	Timeline()
	{
		Timeline(std::chrono::high_resolution_clock::now(),std::chrono::high_resolution_clock::now(),0);
	}
  

  
  
	std::chrono::time_point<std::chrono::high_resolution_clock> getEpoch() { return mEpoch; }
	std::chrono::time_point<std::chrono::high_resolution_clock> getNow() { return mNow; }
	std::chrono::duration<long long, std::milli> getStep() { return mStep; }
	
	std::chrono::duration<long long, std::milli> getTimeSinceEpoch() { return std::chrono::duration_cast<std::chrono::milliseconds>(mNow - mEpoch); }
	
	void setEpoch(std::chrono::time_point<std::chrono::high_resolution_clock> epoch) { mEpoch = epoch; }
	void setNow(std::chrono::time_point<std::chrono::high_resolution_clock> now) { mNow = now; }
	void setStep(long long int step) { mStep = std::chrono::duration<long long, std::milli>(step); }
	void setStep(std::chrono::duration<long long, std::milli> step) { mStep = step; }
	void setPause(bool pause) { mPause = pause; }
	void setSlow(bool slow) { mSlow = slow; }
	
	bool isPaused() { return mPause; }
	bool isSlow() { return mSlow; }	 
	
	void Update(std::chrono::duration<long long, std::milli> step)
	{
		if(!isPaused())
		{
			if(isSlow())
			{
				//use stored step
				setNow(getNow() + getStep());
			}
			else
			{
				//use passed step from GameClock
				setNow(getNow() + step);
			}
		}
	}	 
	
	Timeline& operator=(Timeline& rhs)
	{
		if (this == &rhs) {	return *this; }
		
		this->setPause(rhs.isPaused());
		this->setSlow(rhs.isSlow());
		this->setEpoch(rhs.getEpoch());
		this->setNow(rhs.getNow());
		this->setStep(rhs.getStep());
		
		return *this;
	} 

};

}

#endif
