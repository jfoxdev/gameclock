#include "GameClock.h"

VGTK::GameClock* VGTK::GameClock::mInstance = nullptr;
std::chrono::time_point<std::chrono::high_resolution_clock> VGTK::GameClock::mEpoch;
unsigned int VGTK::GameClock::mTimelineCount = 0;
VGTK::Timeline VGTK::GameClock::mRealTime;
