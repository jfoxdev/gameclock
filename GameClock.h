#ifndef GAME_CLOCK_H
#define GAME_CLOCK_H

#include "Timeline.h"

#include <map>
#include <chrono>
//#include <thread>

//Vatrix Games Tool Kit
namespace VGTK {

class GameClock
{
  public:
	GameClock()
	{
		mEpoch = std::chrono::high_resolution_clock::now();
		mCurrent = mEpoch;
		
		mRealTime.setEpoch(std::chrono::high_resolution_clock::now());
		mRealTime.setNow(mRealTime.getEpoch());
		mRealTime.setStep(0);
	}
	~GameClock() {}
  
	static GameClock* Instance()
	{
		if (mInstance == nullptr)
		{
			mInstance = new GameClock();
		}
		return mInstance;
	}
  
	void Update()
	{
		mLast = mCurrent;
		mCurrent = std::chrono::high_resolution_clock::now();
		
		mEpochDelta = std::chrono::duration_cast<std::chrono::milliseconds>(mCurrent - mEpoch);
		mLastDelta =  std::chrono::duration_cast<std::chrono::milliseconds>(mCurrent - mLast);
		
		mRealTime.setStep(mLastDelta.count());
		mRealTime.Update(mLastDelta);

		//update all the timelines with the curren time step
		std::map<unsigned int, Timeline*>::iterator it;
		for(it = mTimelines.begin(); it!=mTimelines.end(); it++)
		{
			(*it).second->Update(mRealTime.getStep());
		}
		
	}
  
	std::chrono::time_point<std::chrono::high_resolution_clock> getEpoch() { return mRealTime.getEpoch(); }
	std::chrono::time_point<std::chrono::high_resolution_clock> getEpoch(unsigned int id) { return mTimelines[id]->getEpoch(); }
	long long int getTimeSinceEpoch() { return mEpochDelta.count(); }
	long long int getTimeSinceEpoch(unsigned int id) { return mTimelines[id]->getTimeSinceEpoch().count(); }
	long long int getTimeStep() { return mRealTime.getStep().count(); }
	long long int getTimeStep(unsigned int id) { return mTimelines[id]->getStep().count(); }
	std::chrono::time_point<std::chrono::high_resolution_clock> getNow() { return mRealTime.getNow(); }
	std::chrono::time_point<std::chrono::high_resolution_clock> getNow(unsigned int id) { return mTimelines[id]->getNow(); }
  
	void setTimeStep(unsigned int id, long long int duration)
	{
		mTimelines[id]->setStep(duration);
	}
  
	void TogglePause() { mRealTime.setPause(!mRealTime.isPaused()); }
	void TogglePause(unsigned int id)
	{
		mTimelines[id]->setPause(!mTimelines[id]->isPaused());
	} 
	
	bool isPaused() { return mRealTime.isPaused(); }
	bool isPaused(unsigned int id) { return mTimelines[id]->isPaused(); }
	bool isSlow() { return mRealTime.isSlow(); }
	bool isSlow(unsigned int id) { return mTimelines[id]->isSlow(); }
	void ToggleSlow() { mRealTime.setSlow(!mRealTime.isSlow()); }
	void ToggleSlow(unsigned int id) 
	{
		mTimelines[id]->setSlow(!mTimelines[id]->isSlow());
	} 
	
	unsigned int NewTimeline()
	{
		return NewTimeline(mRealTime.getEpoch(), mRealTime.getNow(), mRealTime.getStep().count());
	}
	unsigned int NewTimeline(std::chrono::time_point<std::chrono::high_resolution_clock> epoch,std::chrono::time_point<std::chrono::high_resolution_clock> now, long long int step)
	{
		unsigned int tmp_id = mTimelineCount + 1;
		Timeline* tmp = new Timeline(epoch, now, step);
		mTimelines.insert(std::pair<unsigned int, Timeline*>(tmp_id, tmp)); 
		mTimelineCount++;
		return tmp_id;
	}
	void DestroyTimeline(unsigned int id)
	{
		mTimelines.erase(id);
	}
	void PurgeTimelines()
	{
		mTimelines.clear();
		mTimelineCount = 0;
	}
  
  private:
	static GameClock* mInstance;
	
	std::chrono::time_point<std::chrono::high_resolution_clock> mCurrent;
	std::chrono::time_point<std::chrono::high_resolution_clock> mLast;
	static std::chrono::time_point<std::chrono::high_resolution_clock> mEpoch;

	std::chrono::duration<long long, std::milli> mEpochDelta;
	std::chrono::duration<long long, std::milli> mLastDelta;
	
	static Timeline mRealTime;
	std::map<unsigned int, Timeline*> mTimelines;
	static unsigned int mTimelineCount;
};

}

#endif
